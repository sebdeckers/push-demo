# HTTP/2 Push Demo

## Usage
```
npm start
```

![Screenshot of the demo in action with http2server and multiple HTTP/2 push responses to a single request from Chrome](http://i.imgur.com/Nqv7yka.png)

## Explanation
Demonstration of using ES6 modules without bundling. Does not use Browserify, Webpack, JSPM, etc.

Babel transpiles the ES6 modules to SystemJS syntax. The System.JS loader is added to `index.html` as the entry point. No use is made of the SystemJS syntax in the application code – it's effectively a shim.

The entire app is served using a single request and multiple HTTP/2 Push streams. This eliminates bundling, a performance anti-pattern in HTTP/2.

## Limitations
Use of NPM packages is not currently supported. Suggestions welcome.

## See Also
[npm:http2server](https://www.npmjs.com/package/http2server) is an easy to use HTTP/2 Push server for single page apps.

## Colophon
Made with :heart: in Venice :it: at a [Legalese.io](http://www.legalese.io) workation.
